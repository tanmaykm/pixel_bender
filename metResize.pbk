<languageVersion : 1.0;>

kernel metResize
<   namespace : "com.meeteoric.woopod";     
    vendor : "Meeteoric Technologies Pvt. Ltd.";
    version : 1;
    description : "Bicubic / Bilinear img resize";
>
{
	parameter float2 scale
	<
		defaultValue: float2(1,1);
	>;
	
	parameter bool bicubic
	<
		defaultValue: true;
	>;

	input image4 src;
	output pixel4 dst;

	void evaluatePixel()
	{
		if(bicubic) {
			float2 scaledPt = outCoord() * scale - float2(0.5,0.5);
			float2 pt = floor(scaledPt);

			//http://www.paulinternet.nl/?page=bicubic
			float4 p00 = sample(src,pt+float2(-1.0,-1.0));
			float4 p01 = sample(src,pt+float2(-1.0,0.0));
			float4 p02 = sample(src,pt+float2(-1.0,1.0));
			float4 p03 = sample(src,pt+float2(-1.0,2.0));
			float4 p10 = sample(src,pt+float2(0.0,-1.0));
			float4 p11 = sample(src,pt+float2(0.0,0.0));
			float4 p12 = sample(src,pt+float2(0.0,1.0));
			float4 p13 = sample(src,pt+float2(0.0,2.0));
			float4 p20 = sample(src,pt+float2(1.0,-1.0));
			float4 p21 = sample(src,pt+float2(1.0,0.0));
			float4 p22 = sample(src,pt+float2(1.0,1.0));
			float4 p23 = sample(src,pt+float2(1.0,2.0));
			float4 p30 = sample(src,pt+float2(2.0,-1.0));
			float4 p31 = sample(src,pt+float2(2.0,0.0));
			float4 p32 = sample(src,pt+float2(2.0,1.0));
			float4 p33 = sample(src,pt+float2(2.0,2.0));
			float x = scaledPt.x - pt.x;
			float y = scaledPt.y - pt.y;

			float4 a00 = p11;
			float4 a01 = -p10 + p12;
			float4 a02 = 2.0*p10 - 2.0*p11 + p12 - p13;
			float4 a03 = -p10 + p11 - p12 + p13;
			float4 a10 = -p01 + p21;
			float4 a11 = p00 - p02 - p20 + p22;
			float4 a12 = -2.0*p00 + 2.0*p01 - p02 + p03 + 2.0*p20 - 2.0*p21 + p22 - p23;
			float4 a13 = p00 - p01 + p02 - p03 - p20 + p21 - p22 + p23;
			float4 a20 = 2.0*p01 - 2.0*p11 + p21 - p31;
			float4 a21 = -2.0*p00 + 2.0*p02 + 2.0*p10 - 2.0*p12 - p20 + p22 + p30 - p32;
			float4 a22 = 4.0*p00 - 4.0*p01 + 2.0*p02 - 2.0*p03 - 4.0*p10 + 4.0*p11 - 2.0*p12 + 2.0*p13 + 2.0*p20 - 2.0*p21 + p22 - p23 - 2.0*p30 + 2.0*p31 - p32 + p33;
			float4 a23 = -2.0*p00 + 2.0*p01 - 2.0*p02 + 2.0*p03 + 2.0*p10 - 2.0*p11 + 2.0*p12 - 2.0*p13 - p20 + p21 - p22 + p23 + p30 - p31 + p32 - p33;
			float4 a30 = -p01 + p11 - p21 + p31;
			float4 a31 = p00 - p02 - p10 + p12 + p20 - p22 - p30 + p32;
			float4 a32 = -2.0*p00 + 2.0*p01 - p02 + p03 + 2.0*p10 - 2.0*p11 + p12 - p13 - 2.0*p20 + 2.0*p21 - p22 + p23 + 2.0*p30 - 2.0*p31 + p32 - p33;
			float4 a33 = p00 - p01 + p02 - p03 - p10 + p11 - p12 + p13 + p20 - p21 + p22 - p23 - p30 + p31 - p32 + p33;

			float x2 = x * x;
			float x3 = x2 * x;
			float y2 = y * y;
			float y3 = y2 * y;

			dst =  a00 + a01 * y + a02 * y2 + a03 * y3 +
			       a10 * x + a11 * x * y + a12 * x * y2 + a13 * x * y3 +
			       a20 * x2 + a21 * x2 * y + a22 * x2 * y2 + a23 * x2 * y3 +
			       a30 * x3 + a31 * x3 * y + a32 * x3 * y2 + a33 * x3 * y3;
		}
		else {
			float scale = max(scale[0], scale[1]);
			dst = sample(src, outCoord() * scale); // bilinear scaling
		}
	}
}

